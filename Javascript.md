## Variables

| Key   | Descriptions                                       |
| ----- | -------------------------------------------------- |
| `const` | Immutable variable and block scoped, initialized   |
| `let`   | Mutable variable and block scoped, non initialized |
| `var`   | Global scoped variables, can be initialized or not |

## Set

- Returns an **array** with unique values
- Define with `new Set(arr)`

## Map (Key value pairs)

| Methods       | Description                             |
| ------------- | --------------------------------------- |
| `map.get(k)`    | Get the value of index key              |
| `map.set(k,v)`  | Set the value of index key              |
| `map.size`      | Returns number of item(k, v) in the map |
| `map.delete(k)` | Delete the value and key                |

- Map is not iterable
- Map remembers the insertion order
- Define with `new Map([[k1,v1], [k2,v2]])`

## Arrow functions

- Must be declared before call
- `this` inside refers to the current context of the scope from where the function is called
- If a method uses `return this` then the method can be chained like promise

## Rest, Spread and Destructuring

| Parameters     | Description                                               |
| -------------- | --------------------------------------------------------- |
| `...values`    | Taking everything and put it into an array (used in args) |
| `...obj1`      | take an array and flatten into elements                   |
| `{var1, var2}` | Destructuring, pick property from an object               |
| `[var1, var2]` | Destructuring, pick element from an array                 |

## Literals

- string literals
  - can be shorten with `This is a ${var}`
- object literals
  - same named value and variables can be shorten with `onlyName`
  - property function can be shorten with `fnName(){}`

## Promise

- Automatically get _resolved_, _rejected_ injected into the callback
- _resolved_ conveyed to _then_ block
- _rejected_ conveyed to _catch_ block
- If the incoming data can not be certain that its a value or another promise

```js
Promise.resolve(incoming).then(res=> "Promise execution result")
```

- To call multiple promise together

```js
Promise.all(promise1(), promise2()).then(res=> "All done")
```

## Array Methods

| Methods   | Description                                                                                   |
| --------- | --------------------------------------------------------------------------------------------- |
| `forEach` | Run for loops in blocked scope                                                                |
| `map`     | Returns a modified new **array** or isolated key values array                                 |
| `find`    | Returns first single **object** or element                                                    |
| `filter`  | Returns an **array** where condition is true                                                  |
| `indexOf` | Given a `find` object return its index in the array, returns -1 if not found                  |
| `every`   | Iterate over every object and flag it bool based on condition then return a single **bool** by `and` operator |
| `some`    | Iterate over every object and flag it bool based on condition then return a single **bool** by `or` operator |
| `reduce`  | Iterate over array and return cumulative single value                                         |

Reduce Syntax = `array.reduce((accumulator, current) => accumulator + current, initial_value)`

## String Methods

| Methods            | Description                                           |
| ------------------ | ----------------------------------------------------- |
| `repeat("str", n)`   | Repeat n number of "str"                              |
| `startsWith("str")`  | Returns bool if "str" match at start                  |
| `endsWith("str")`    | Returns bool if "str" match at end                    |
| `includes("str")`    | Returns bool if case sensitive "str" matches anywhere |
| `padStart(n, "str")` | Tab n column with "str" from start                    |
| `padEnd(n, "str")`   | Tab n column with "str" at the end                    |

## Objects Methods

| Methods        | Description                                              |
| -------------- | -------------------------------------------------------- |
| `Object.assign`  | Merge two obj and returns a new obj, also modify sources |
| `Object.keys`    | Return an array of all keys in an obj                    |
| `Object.values`  | Return an array of all values in an obj                  |
| `Object.entries` | Return an obj of sub arrays `{[k1, v1], [k2,v2]}`        |

Tip: Make an obj iterable

```js
obj {
 *[Symbol.Iterator]() {
  for (let key in this)
   yield {[key]: this[key]}
 }
}
```

## Generator

- Useful for iterate one at a time
- Syntax = `function* NAME() { yield; }`
- use `yield` to return the value
- use `yield*` to parse another generator
- It returns an object with two parameters `{value:data, done:bool}`
- `generatorFn.next()` for iterate and yield next until **done** is **true**

## JSDoc

- To activate use `/** .. */`,
- Parameter listing in jsdoc `@param {type} : Description`
