## Tsconfig

| cli command                         | Description                                             |
| ----------------------------------- | ------------------------------------------------------- |
| `tsc`                               | Compile ts -> js                                        |
| `tsc --version`                     | Show typescript version                                 |
| `tsc -w`                            | Auto re-compile on change of ts files                   |
| `tsc --init`                        | Initialize tsconfig file and mark root where it resides |
| `tsc --watch --preserveWatchoutput` | Auto re compile but don't clear the console             |

- `tsconfig` will take preference when compiling in order of **files -> excludes -> includes**
- `/**/*.ts` pattern for recursively grab all ts files
- `@ts-ignore` = The next line will be ignored by the compiler for type checking
- `@ts-expect-error` = The next line expect a type mismatch. If type matches compiler error throws.

## Data types

| types      | Description                                                   | Example                     |
| ---------- | ------------------------------------------------------------- | --------------------------- |
| `string`   | Text literals                                                 | `"str"`                     |
| `number`   | Integers or Floats                                            | `23, 0.2`                   |
| `bool`     | Boolean                                                       | `true, false`               |
| `array`    | Arrays of same type                                           | `[23,34,12]`                |
| `tuple`    | Arrays of different type                                      | `["str", true, 23]`         |
| `object`   | Object key value pairs                                        | `{name:string, age:number}` |
| `type`     | Any arbitrarily declaration                                   |                             |
| `struct`   | Object type, more powerful than type                          |                             |
| `enum`     | Object with numerical representation                          |                             |
| `generics` | Arbitrary type that converts the preceding class or functions | `<T>`                       |
| `any`      | Any types inferred by the compiler,(avoid as much as possible)|                             |
| `unknown`  | Same as `any` but this time type narrowing is up to the coder   |                             |
| `void`     | Expect nothing in return                                      |                             |
| `never`    | Throw in use and function ends without  execution             |                             |
| `|`        | Union type, merging together                                  |                             |
| `&`        | Intersection type,                                            |                             |
| null       |                                                               |                             |
| undefined  |                                                               |                             |
| symbol     |                                                               |                             |

- _Javascript_ types are _CAPS_ and _Typescript_ types are _SMALL_
- `type` generally use for tuple or array or complex types representation
- Separator can be added for better readability = 1_121_000 as same as 1121000
- Casting into another type = `<Type>otherType` or `otherType as Type`

## Type guard

| key          | Description              | Example               |
| ------------ | ------------------------ | --------------------- |
| `as`         | Enforce to a type        | `var1 as string`      |
| `in`         | Part of object           | `"something" in obj1` |
| `typeof`     | Exact copy of the object | `typeof obj1`         |
| `keysof`     | Keys of object           | `keysof obj1`         |
| `instanceof` | Exact copy of class      | `instanceof class1`   |

- `typeof` refers to exact copy of the object

```typescript
const someObj = {name: 'Enki', age: 999}
type someType = typeof someObj;
```

- `keysof` refers to any keys / properties of the object

```typescript
type someType = keysof someObj;
```

- Add type constraints in generics by using interface `<T extends Type>`

```typescript
interface lengthy {
 length: number;
}
function myFn<T extends lenghty, U extends keyof T>(obj: T, key: U)
```

## Visibility & Modifiers

| keyword     | Description                                                      |
| ----------- | ---------------------------------------------------------------- |
| `public`    | Mutable and exposed                                              |
| `private`   | Only can be used inside declaration class                        |
| `protected` | Can be used in inherited class but not exposed                   |
| `readonly`  | Immutable and only initialize via constructor                    |
| `abstract`  | Class that can not be initialized but can inherit                |
| `static`    | Class methods that can be called without instantiating the class |
| `declare`   | Initialize a placeholder and let ts know.                        |

## Built in

| fn                             | Description                                              |
| ------------------------------ | -------------------------------------------------------- |
| `Readonly<T>`                  | Iterate over an object and return all keys as _readonly_ |
| `Partial<T>`                   | Iterate over an object and return all keys as _optional_ |
| `Required<T>`                  | Iterate over an object and return all keys as _required_ |
| `Pick<T, K extends keyof T>`   | Returns specific keys from an object                     |
| `Record<K extends keyof T, T>` | Returns dictionaries with enforced concrete type                                                         |

## interface

- More powerful than `type`, generally use for object representation
- Interface index signature = `[key:number]: string`, It is used to include indexing on interface. e.g: `1: str`, It makes the interface dynamic.
- interface inherits via `implements`

## class

- Abstract class can not be instantiated, only can be inherited
- Class inherits via `extends`
- Property access can be given via function with `get` modifier so in outer scope in does not need to call extensively

```typescript
class A {
 data: number;
 get dataInfo(): number {
  return this.data;
 }
}
const a = new A(2);
const datainfo = A.dataInfo;
```

- `#name: string` = It is a class field. It is hard private so inherited class can have the same name property because child class don't see this class field.
- Nullish coalescing operator

```typescript
class A {
 name: string;
 constructor(rawInput: string) {
  this.name = rawInput ?? "Default Name";
 }
}
```

- Nullish coalescing is fallback to default without specifying default value in args.
- `!` refers that the coder know the return will never be null or undefined

## functions

- For function overloading have all the overloading signature on top of the file.
- Make any arguments optional

```typescript
function fn(args?: string)
```

- Make any arguments required

```typescript
function fn(args!: string)
```

- Both optional and required args can have `-` / `+` infront of them to inform taking away or adding the requirements to the args

## decorator

- Decorator are functions used to modify / change / update properties
- It runs without invoking during class definition, not during instantiation
- signature syntax = `@fn`
- Based on place where it is used it gets different arguments. Receiving arguments can be ignored via `_`

  1. class

     - `function fn(constructor: Function)`

  2. property

     - `function fn(target: any, propName: string)`

  3. functions including get accessor

     - `function fn(target: any, propName: string, descriptor: propertyDescriptor)`

## Split codes

1. Namespace

```typescript
// definition
namespace SomeName {

}

// import example
/// <reference path="file.ts" />
```

2. es6 export import rules applied
