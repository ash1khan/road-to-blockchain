# Road to a blockchain job <!-- omit in toc -->

> Collaboration of Ashk And Dimitry to the journey from none to infinity. This page serves as a tiny note taking app of what we know or learning and necessary resources links.

# Table of Contents <!-- omit in TOC -->

- [1. :mortar_board: Qualification Required](#1-mortar_board-qualification-required)
  - [1.1. **Web development** (1 year of experience)](#11-web-development-1-year-of-experience)
    - [1.1.1. _Backend_](#111-backend)
    - [1.1.2. _API_](#112-api)
    - [1.1.3. _Database_](#113-database)
    - [1.1.4. _Frontend_](#114-frontend)
    - [1.1.5. _DevOps_](#115-devops)
    - [1.1.6. _Others paired with web dev_](#116-others-paired-with-web-dev)
  - [1.2. **Blockchain knowledge**(6 months of experience)](#12-blockchain-knowledge6-months-of-experience)
    - [1.2.1. _Consensus_](#121-consensus)
    - [1.2.2. _Cryptography_](#122-cryptography)
    - [1.2.3. _Data structure_](#123-data-structure)
    - [1.2.4. _Others along with blockchain_](#124-others-along-with-blockchain)
  - [1.3. **Smart Contracts** (1 year of experience)](#13-smart-contracts-1-year-of-experience)
    - [1.3.1. _Solidity_ (Learn to create)](#131-solidity-learn-to-create)
    - [1.3.2. _Tools_ (Learn to compile and deploy)](#132-tools-learn-to-compile-and-deploy)
    - [1.3.3. _Audits_ (Learn to test)](#133-audits-learn-to-test)
    - [1.3.4. _UI_ (Learn to expose for enduser)](#134-ui-learn-to-expose-for-enduser)
    - [1.3.5. _Others_](#135-others)
    - [1.3.6. _Protocols_](#136-protocols)
- [2. :speech_balloon: Fit in or get exposures](#2-speech_balloon-fit-in-or-get-exposures)
- [3. :chart_with_upwards_trend: Stay upto date](#3-chart_with_upwards_trend-stay-upto-date)
- [4. :heavy_dollar_sign: Income Sources](#4-heavy_dollar_sign-income-sources)
- [5. :question: Questions or issues](#5-question-questions-or-issues)
- [6. :school_satchel: Helpful Resources](#6-school_satchel-helpful-resources)
- [7. :memo: Notes](#7-memo-notes)
- [8. :briefcase: Portfolio projects](#8-briefcase-portfolio-projects)
- [9. :rocket: Post learning activities](#9-rocket-post-learning-activities)

---

## 1. :mortar_board: Qualification Required

Here is a compiled list of technology stacks a blockchain developer should know. This list is compiled after following some job post from `linkedin` `cryptojobs` `angellist`

### 1.1. **Web development** (1 year of experience)

#### 1.1.1. _Backend_

Any sort of backend knowledge is required. The followings are list of what we know already or need a refresher review and what need to learn in future. A checkmark will be placed along with date of completion after reviewing finish or learn a new subject. Subject speicific

- [ ] **Django** (Ashk|Review)
  - _Resources_
    1. [Official Django reference](https://docs.djangoproject.com/en/4.0/ref/)
    2. [Ultimate Django Series](https://tutsnode.net/the-ultimate-django-series-part-1/)
    3. [Udemy - Django 4 masterclass 2022 Build web app with django](https://getfreecourses.co/django-4-masterclass-2022-build-web-apps-with-django/)
    4. [Vue3, Nuxt and Django A rapid guide - advanced](https://www.freetutorialsus.com/vue-3-nuxt-js-and-django-a-rapid-guide-advanced/)
    5. [Ultimate Authentication with django and vue js](https://tutsnode.net/the-ultimate-authentication-course-with-django-and-vuejs/)
    6. [Build a REST API like a boss](https://tutsnode.net/creating-python-apis-like-a-boss-the-django-rest-framework/)
  - _Project Ideas_
     1. TODO: Fear&Greed Index (Files needed will be in a separate folder)

- [ ] **Fastapi** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Golang** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Rust** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] 5. **Nodejs** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:

#### 1.1.2. _API_

The communication between blockchain layer and frontend layer does not depend on API. Nevertheless job requires some kind of understanding of any type of api.

- [ ] **REST** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Graphql** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **gRPC** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:

#### 1.1.3. _Database_

Not necessary for blockchain but essential to know how backend CRUD with different types of SQL

- [ ] **Postgres** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Mongodb** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Reddis** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Cassandradb** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:

#### 1.1.4. _Frontend_

Must have knowledge of how frontend (dapps) communicate with smart contracts and connects to blockchain layer

- [ ] **Javascript** (Ashk|Review)
  - Resources
    1. [AshK study exerpt](Javascript.md)
  - Project Ideas
    1. TODO:
- [ ] **Typescript** (Ashk|Review)
  - Resources
    1. [AshK study exerpt](typescript.md)
  - Project Ideas
    1. TODO:
- [ ] **Vue/Nuxt** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **React** (Most used)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:

#### 1.1.5. _DevOps_

Version control system is a must. Containerization is not must but good to have to show off and stand out.

- [ ] **Git** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Gitlab ci/cd** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Github actions** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Docker** (Ashk|Review)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:
- [ ] **Kubernetes** (Nice to have)
  - Resources
    1. TODO:
  - Project Ideas
    1. TODO:

#### 1.1.6. _Others paired with web dev_

Necessary to answer in job interview.

- [ ] **Algorithm**
  - Resources
    1. TODO:
- [ ] **Design Patterns** (Python)
  - Resources
    1. TODO:

---

### 1.2. **Blockchain knowledge**(6 months of experience)

Topics need to know in order to tackle question in interview or have a deep understanding on blockchain technology

#### 1.2.1. _Consensus_

- Resources
    1. [Consensus Encyclopedia](https://tokens-economy.gitbook.io/consensus/)
    2. [SImplyExplained from Youtube](https://www.youtube.com/watch?v=M3EFi_POhps)
    3. [MIT Opensource from Youtube](https://www.youtube.com/watch?v=mBdrvfytLDQ&t=1206s)
    4. [TechInAsia from Youtube](https://www.youtube.com/watch?v=ojxfbN78WFQ&t=2s)
    5. [WhiteboardCrypto from Youtube](https://www.youtube.com/watch?v=3QCykHU89To)
    6. [AshK study excerpt](Consensus.md)

#### 1.2.2. _Cryptography_

- [ ] Public key encryption
- [ ] Hash function

#### 1.2.3. _Data structure_

- [ ] Merkle tree

#### 1.2.4. _Others along with blockchain_

- Things to cover
  - [ ] Decentralization (Part of trilemma)
  - [ ] Scalability (Part of trilemma)
  - [ ] Security (Part of trilemma)
  - [ ] Forking
  - [ ] How DAO works
  - [ ] How Defi works
  - [ ] How NFT works
  - [ ] Major fraudulents or events on the blockchain history
- Resources
  1. [Finematics Youtube](https://www.youtube.com/c/Finematics)
  2. [James Bachini Youtube](https://www.youtube.com/c/JamesBachini/videos)
  3. [Unstoppable Domains Youtube](https://www.youtube.com/c/UnstoppableDomains/videos)
  4. [Binance academy Youtube](https://www.youtube.com/c/BinanceAcademy/videos)
  5. [Simply Explained Youtube](https://www.youtube.com/playlist?list=PLzvRQMJ9HDiQF_5bEErheiAawrJ-2zQoI)
  6. [Whiteboard crypto Youtube](https://www.youtube.com/c/WhiteboardCrypto)

### 1.3. **Smart Contracts** (1 year of experience)

Skills needed as a must for blockchain progeammers.

#### 1.3.1. _Solidity_ (Learn to create)

- Things to cover
  - [ ] Programming over view and concepts
  - [ ] Common Hacks
  - [ ] ERC standards and Openzeppline
  - [ ] Design patterns
  - [ ] Gas optimization technique
  - [ ] Testing smart contract (Not E2E)
- Resources
    1. [Solidity 0.4 basic from Will it scale Youtube](https://www.youtube.com/playlist?list=PL16WqdAj66SCOdL6XIFbke-XQg2GW_Avg)
    2. [Solidity 0.5 from Eat the Block Youtube](https://www.youtube.com/playlist?list=PLbbtODcOYIoE0D6fschNU4rqtGFRpk3ea)
    3. [Solidity 0.5 from Smart Contract Programmer Youtube](https://www.youtube.com/playlist?list=PLO5VPQH6OWdULDcret0S0EYQ7YcKzrigz)
    4. [Hack solidity 0.6 - 0.7 from Smart Contract Programmer](https://www.youtube.com/playlist?list=PLO5VPQH6OWdWsCgXJT9UuzgbC8SPvTRi5)
    5. [Solidity 0.8 from Smart Contract Programmer Youtube](https://www.youtube.com/playlist?list=PLO5VPQH6OWdVQwpQfw9rZ67O6Pjfo6q-p)
    6. [Solidity Applications from Smart Contract Programmer](https://www.youtube.com/playlist?list=PLO5VPQH6OWdVfvNOaEhBtA53XHyHo_oJo)
    7. [Complete Solidity from Derek Banas Youtube](https://www.youtube.com/watch?v=3g2WT2jms_k)
    8. [Solidity and Smart Contracts bootcamp from Patrick Collins Youtube](https://www.youtube.com/watch?v=M576WGiDBdQ)

#### 1.3.2. _Tools_ (Learn to compile and deploy)

- [ ] Truffle
- [ ] Hardhat
- [ ] Local test networks (Ganache)
- [ ] Wallets (Metamask)
- [ ] Oracles (chainlink, moralis, infura)

#### 1.3.3. _Audits_ (Learn to test)

- [ ] mythx
- [ ] slither
- [ ] echidna

#### 1.3.4. _UI_ (Learn to expose for enduser)

- [ ] Web3 js
- [ ] Ethers js

#### 1.3.5. _Others_

- [ ] IPFS Storing and fetching
- [ ] Current Limitations
- [ ] New updates
- [ ] Sharding

#### 1.3.6. _Protocols_

- [ ] Binance smart chain
- [ ] Matic (Polygon networks)
- [ ] AAVE
- [ ] Compound
- [ ] Uniswap

---

## 2. :speech_balloon: Fit in or get exposures

We are confident enough now to get into the industry. Time to show off to the world

- [ ] Build apps for portfolio
  - Heroku deploy
  - Github pages deploy
  - Netlify deploy
  - AWS deploy
  - Digital Ocean deploy
  - Linode deploy
- [ ] Participate and help others in
  - groups telegram
  - discord communities
  - slack channels
- [ ] Bug bounty participation
- [ ] Contribute to open source
  - github
  - gitlab
- [ ] Certification from
  - Consensys Academy
- [ ] Linkedin resume and connections
- [ ] Blogs posting about what i know in blockchain
  - hackernoon,
  - medium,
  - ghost
- [ ] Personal website

---

## 3. :chart_with_upwards_trend: Stay upto date

The technology is shifting rapidly. Need to keep eyes open all the time.

- Listen to podcast
- Read crypto news
- Youtube

## 4. :heavy_dollar_sign: Income Sources

:mega: Doesn't matter if we fail 1000 times. 1001st will lead us to our destination and that will matter.

From here we can pick up work or hunt for jobs. Later on I want to do an automation with scrappy or beautiful soup library to automate job applying. It can be a viable project for the portfolio too.

1. **Freelancing platforms**

2. **Job board**
   - [Linkedin](https://www.linkedin.com/)
   - [ZipRecruiter](https://www.ziprecruiter.com/Jobs/Blockchain)
   - [Cryptojoblist](https://cryptojobslist.com/)
   - [CryptoJobs](https://cryptojobs.com/)
   - [Pompcryptojobs](https://pompcryptojobs.com/)
   - [Cryptocurrencyjobs](https://cryptocurrencyjobs.co/)
   - [Crypto.Jobs](https://crypto.jobs/)
   - [Cryptojobs.careers](https://cryptojobs.careers/)
   - [Ingame recruitement](https://www.ingamerecruitment.com/)

3. **Groups and communities**
    - Slack channels
    - Telegram groups
    - Discord Communities
    - Sub reddits

## 5. :question: Questions or issues

List of Obstacles we are facing

## 6. :school_satchel: Helpful Resources

Important links here

## 7. :memo: Notes

Quick notes over any topics

## 8. :briefcase: Portfolio projects

Published projects are listed here

## 9. :rocket: Post learning activities
