# Consensus

A consensus is a mechanism to agree upon a decision. The blockchain is an open ledger where everything happens in transactions. For example creating an asset into existence, sending money to an account. several transactions packs together by cryptography and send to a node to verify the transactions. To verify the transactions the node uses an algorithm model. That model is the procedure for coming to agreement that the transaction is valid. That is called Consensus model

**Types**

Transactions verification is done by finding the nonce.
There are many type of consensus model to choose how to find the nonce.

1. _Proof of Work (PoW)_

- What: Every one is racing to solve the nonce, who comes first with the nonce will be rewarded.
- Pros: Secure and scalable. proved to be consistent.
- Cons: Very operational power consuming. Low transaction limit
- Whose are using it: **Bitcoin**, **Ethereum**

2. _Proof of Stack (PoS)_

- What: Randomly select the validators who will verify the block. To become a validator the node must stack some amount determined by the protocols.
- Pros: Low power consumption. Inexpensive and increased transaction speed.
- Cons: With large stake can gain advantage over other validators.
- Whose are using it: **Cardano**, **Tezos**, **Algorand**

3. _Delegated Proof of Stack (DPoS)_

- What: Same as Proof of Stack. Validator can vote for another validator to do the hashing on behalf of the selected validator.
- Pros: Validators don't need to have costly hardware to do the hashing. Better reward distributions.
- Cons: Cartel Formation, Easier for 51% attack
- Whose are using it: **Ark**, **Nano**, **Cardano**, **EOS**, **Tezos**

4. _Proof of Authority (PoA)_

- What: Selected authority can only verify blocks
- Pros: No chance of fault verification. Very fast
- Cons: Not decentralize. Authority is not anonymous.
- Whose are using it: **Hyperledger**, **Kovan testnet**

5. _Proof of Spacetime (PoSt)_

- What: Using real hardware to store and verify unique data shards for a period of time. For that period of time they get incentives from the network. Verification is done by randomly chosen miners that holds the actual data. A modified version of _Proof of Capacity (PoC)_
- Pros: No gains by harming the network
- Cons: The rewarding cost is proportional to amount of data storage and time.
- Whose are using it: **Filecoin**, **Bittorrent coin**, **Chia**

6. _Proof of Attendance (PoAP) protocol_

- What: Be a validator by attendance. Which is collecting a NFT or earning an unique badge for attending an event organized by host.
- Pros: Solves the anonymity problems. Irrefutably which means impossible to disprove.
- Cons: Requires ERC 721. Not applicable in broad range of business.
- Whose are using it: **SushiSwap**, **Virtual events on Metaverse**

7. _Proof of Weight (PoWeight)_

- What: Modified version of _Proof of Stake_. Based on number of factors randomly chosen the next block discoverer. The main factor is how much value or weight you hold in terms of the network (network token, elapsed time, etc).
- Pros: Customizable and scalable
- Cons: Incentivizing is challenging
- Whose are using it: **Algorand**

8. _Proof of Burn (PoB)_

- What: Miners burn some tokens to reach the consensus.
- Pros: virtual mining rig, no physical hardware involved, Stops inflation
- Cons: Not proven to work as it is very new and no one utilize it yet
- Whose are using it: no one.

9. _Proof of History (PoH)_

- What: Added complexity with time on the varification process. It uses Varifiable Delay Functions rather than finding nonce.
- Pros: Single core CPU can compute VDF. Extremely Fast, Low fees.
- Cons: Not put to the test for wide scale scenario
- Whose are using it: **Solana**

10. _Practical Byzantine Fault Tolerance (PBFT)_

- What: Works in asynchronously verifying blocks.
- Pros: Extremely fast and highly customizable
- Cons:
- Whose are using it: **Hyperledger fabric**, **Ziliqa**

11. _Unique Node List / Byzantine Fault Tolerance (BFT)_

- What: Every Node hold their unique list or responsible for their own chain. Short message send to each nodes to verify its authenticity
- Pros:
- Cons:
- Whose are using it: **ripple**

12. _Direct Acyclic Graph (DAG)_

- What: To verify a transaction a node must verify previous two transactions
- Pros: Extremely small transaction can be processed, low fees, very lightweight
- Cons: No smart contract , 1/3 vulnerabilities
- Whose are using it: **IOTA**
