import re
import datetime as dt
import pandas_datareader as web
import numpy as np
import pandas as pd


def volatility(cryptos):
    c = 0
    for crypto in cryptos:
        start = dt.datetime.now() - dt.timedelta(days=20)
        end = dt.datetime.now()
        data = web.DataReader(crypto, 'yahoo', start, end)
        data['average'] = (data['High'] + data['Low'] + data['Open'] + data['Close'])/4
        mean = data['average'].mean()
        downside = data[data['average'] < mean]
        downside = downside['average'].std() / mean
        downside = round(downside, 3)
        c += (100*(downside))**2
    return c/10



def clean_tweet(tweet):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())

